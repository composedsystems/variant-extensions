# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.1.0] 2022-02-21
### Fixed
- Enum to value string conversion (enum was not supported)

## [1.0.1] 2019-02-14
### Changed
- Separated all compiled code from source
- Whitelisted Source folder only

## [1.0.0] 2019-01-23
### Release Note
First release of this component through GPM
### Changed
- Renamed a public method for consistency and to make quick-dropping easier
- Made one method private